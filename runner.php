<?php
chdir(__DIR__);

set_error_handler('errorHandler');

function errorHandler($severity, $message, $filename, $lineno) {
    if (error_reporting() == 0) {
        return;
    }

    if (error_reporting() & $severity) {
        throw new ErrorException($message, 0, $severity, $filename, $lineno);
    }
}

require 'vendor/autoload.php';

$config = include 'config/config.default.php';

$env = getenv('ENVIRONMENT') ?: 'development';
$envConfigFileName = 'config/config.' . $env . '.php';

if (file_exists($envConfigFileName)) {
    $envConfig = include $envConfigFileName;
    
    $config = array_replace_recursive($config, $envConfig);
}

$dispatcher = new \Updashd\Runner($config);
$dispatcher->run();