<?php
$workspaceDir = __DIR__ . '/../../';
$workspaceDir = str_replace('/', DIRECTORY_SEPARATOR, $workspaceDir);

return [
    'process' => [
        'rescheduler' => [
            'name' => 'Rescheduler',
            'cwd' => $workspaceDir . 'updashd-rescheduler',
            'cmd' => 'php rescheduler.php',
            'instance_count' => 1
        ],
        'agent' => [
            'name' => 'Agent',
            'cwd' => $workspaceDir . 'updashd-agent',
            'cmd' => 'php agent.php -ai',
            'instance_count' => 1
        ],
//        'indexer' => [
//            'name' => 'Indexer',
//            'cwd' => $workspaceDir . 'updashd-indexer',
//            'cmd' => 'php indexer.php -i',
//            'instance_count' => 1
//        ],
        'dispatcher' => [
            'name' => 'Dispatcher',
            'cwd' => $workspaceDir . 'updashd-dispatcher',
            'cmd' => 'php dispatcher.php',
            'instance_count' => 1
        ],
        'registrar' => [
            'name' => 'Registrar',
            'cwd' => $workspaceDir . 'updashd-registrar',
            'cmd' => 'php registrar.php',
            'instance_count' => 1
        ],
        'recorder' => [
            'name' => 'Recorder',
            'cwd' => $workspaceDir . 'updashd-recorder',
            'cmd' => 'php recorder.php',
            'instance_count' => 1
        ],
		'worker' => [
            'name' => 'Worker',
            'cwd' => $workspaceDir . 'updashd-worker-php',
            'cmd' => 'php worker.php',
            'instance_count' => 5
        ],
        'notifier' => [
            'name' => 'Notifier',
            'cwd' => $workspaceDir . 'updashd-notifier-php',
            'cmd' => 'php notifier.php',
            'instance_count' => 1
        ],
        'pusher' => [
            'name' => 'Pusher',
            'cwd' => $workspaceDir . 'updashd-pusher',
            'cmd' => 'php pusher.php',
            'instance_count' => 1
        ],
        'watchdog' => [
            'name' => 'Watchdog',
            'cwd' => $workspaceDir . 'updashd-watchdog',
            'cmd' => 'php watchdog.php',
            'instance_count' => 1
        ],
        'db-maintenance' => [
            'name' => 'DB Maintenance',
            'cwd' => $workspaceDir . 'updashd-db-maintenance',
            'cmd' => 'php dbcleanup.php',
            'instance_count' => 1
        ]
    ]
];