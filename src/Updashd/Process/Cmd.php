<?php

namespace Updashd\Process;

class Cmd {
    const STATUS_FIELD_COMMAND = 'command';
    const STATUS_FIELD_PROCESS_ID = 'pid';
    const STATUS_FIELD_IS_RUNNING = 'running';
    const STATUS_FIELD_IS_SIGNALED = 'signaled';
    const STATUS_FIELD_IS_STOPPED = 'stopped';
    const STATUS_FIELD_EXIT_CODE = 'exitcode';
    const STATUS_FIELD_TERMINATE_SIGNAL = 'termsig';
    const STATUS_FIELD_STOP_SIGNAL = 'stopsig';

    // Settings
    protected $command = '';
    protected $descritorSpecifications = [];
    protected $currentWorkingDirectory = '';
    protected $environmentVariables = [];
    protected $otherOptions = [];

    // Runtime variables
    protected $pipes = [];
    protected $resource;
    protected $isRunning = false;
    protected $stdIn;
    protected $stdOut;
    protected $stdErr;

    public function __construct ($cmd, $cwd = null, $env = null) {
        $this->setCommand($cmd);
        $this->setCurrentWorkingDirectory($cwd);
        $this->setEnvironmentVariables($env);

        $dsc = new Descriptor();
        $this->setDescritorSpecifications($dsc->getDescriptorMapping());
    }

    public function start () {
        $this->resource = proc_open(
            $this->getCommand(),
            $this->getDescritorSpecifications(),
            $this->pipes,
            $this->getCurrentWorkingDirectory(),
            $this->getEnvironmentVariables(),
            $this->getOtherOptions()
        );

        $this->isRunning = is_resource($this->resource) && $this->getStatusIsRunning();

        return $this->getStatusPid();
    }

    public function end () {
        $retVal = proc_close($this->resource);

        $this->isRunning = false;

        return $retVal;
    }

    public function kill ($signal = 15) {
        return proc_terminate($this->resource, $signal);
    }

    public function getStatusCommand () {
        return $this->getStatusField(self::STATUS_FIELD_COMMAND);
    }

    public function getStatusPid () {
        return $this->getStatusField(self::STATUS_FIELD_PROCESS_ID);
    }

    public function getStatusIsRunning () {
        return $this->getStatusField(self::STATUS_FIELD_IS_RUNNING);
    }

    public function getStatusIsStopped () {
        return $this->getStatusField(self::STATUS_FIELD_IS_STOPPED);
    }

    public function getStatusExitCode () {
        return $this->getStatusField(self::STATUS_FIELD_EXIT_CODE);
    }

    public function getStatusTerminateSignal () {
        return $this->getStatusField(self::STATUS_FIELD_TERMINATE_SIGNAL);
    }

    public function getStatusStopSignal () {
        return $this->getStatusField(self::STATUS_FIELD_STOP_SIGNAL);
    }

    public function getStatusField ($key = null) {
        if (is_resource($this->resource)) {
            $res = proc_get_status($this->resource);

            return array_key_exists($key, $res) ? $res[$key] : $res;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getCommand () {
        return $this->command;
    }

    /**
     * @param string $command
     */
    public function setCommand ($command) {
        $this->command = $command;
    }

    /**
     * @return array
     */
    public function getDescritorSpecifications () {
        return $this->descritorSpecifications;
    }

    /**
     * @param array $descritorSpecifications
     */
    public function setDescritorSpecifications ($descritorSpecifications) {
        $this->descritorSpecifications = $descritorSpecifications;
    }

    /**
     * @return string
     */
    public function getCurrentWorkingDirectory () {
        return $this->currentWorkingDirectory;
    }

    /**
     * @param string $currentWorkingDirectory
     */
    public function setCurrentWorkingDirectory ($currentWorkingDirectory) {
        $this->currentWorkingDirectory = $currentWorkingDirectory;
    }

    /**
     * @return array
     */
    public function getEnvironmentVariables () {
        return $this->environmentVariables;
    }

    /**
     * @param array $environmentVariables
     */
    public function setEnvironmentVariables ($environmentVariables) {
        $this->environmentVariables = $environmentVariables;
    }

    /**
     * @return array
     */
    public function getOtherOptions () {
        return $this->otherOptions;
    }

    /**
     * @param array $otherOptions
     */
    public function setOtherOptions ($otherOptions) {
        $this->otherOptions = $otherOptions;
    }
}