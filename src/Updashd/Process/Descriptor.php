<?php
namespace Updashd\Process;

class Descriptor {
    const STDIN = 0;
    const STDOUT = 1;
    const STDERR = 2;

    const TYPE_PIPE = 'pipe';
    const TYPE_FILE = 'file';

    const FIELD_POS_TYPE = 0;
    const FIELD_POS_FILE_OR_DIR = 1;
    const FIELD_POS_FLAGS = 2;

    protected $descriptorMapping = [];

    public function __construct () {
        $this->setDescriptor(self::STDIN, [self::TYPE_PIPE, 'r']);  // Child should read from STDIN
        $this->setDescriptor(self::STDOUT, [self::TYPE_PIPE, 'w']); // Child should write to STDOUT
        $this->setDescriptor(self::STDERR, [self::TYPE_PIPE, 'w']); // Child should write to STDOUT
    }

    public function setDescriptor ($descriptorId, $descriptor) {
        $this->descriptorMapping[$descriptorId] = $descriptor;
    }

    public function getDescriptorMapping () {
        return $this->descriptorMapping;
    }

    public function setDescriptorMapping ($mapping) {
        $this->descriptorMapping = $mapping;
    }
}