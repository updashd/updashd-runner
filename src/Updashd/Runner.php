<?php
namespace Updashd;

use Updashd\Process\Cmd;
use Updashd\Process\Descriptor;

class Runner {

    private $config;

    /** @var Cmd[] */
    private $running = [];

    private $errorCounts = [];

    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);
    }

    /**
     * Run the worker
     */
    public function run () {
        $processes = $this->getConfig('process');

        foreach ($processes as $processKey => $process) {
            $name = $this->getProcessField($process, 'name', $processKey);
            $cmd = $this->getProcessField($process, 'cmd', 'echo');
            $cwd = $this->getProcessField($process, 'cwd', getcwd());
            $env = $this->getProcessField($process, 'env', null);

            $count = $this->getProcessCount($process);

            for ($i = 1; $i <= $count; $i++) {
                $logDir = __DIR__ . '/../../logs/';
                $logDir = str_replace('/', DIRECTORY_SEPARATOR, $logDir);

                $outputLog = $this->getProcessField($process, 'output_log', $logDir . $processKey . '_output_log');
                $outputLog .= '_' . $i . '.txt';

                $errorLog = $this->getProcessField($process, 'error_log', $logDir . $processKey . '_error_log');
                $errorLog .= '_' . $i . '.txt';

                $cmdObj = new Cmd($cmd, $cwd, $env);

                $this->log('Starting ' . $name . '(' . $i  . ')...', true, false);

                $dsc = new Descriptor();
                $dsc->setDescriptor($dsc::STDOUT, [$dsc::TYPE_FILE, $outputLog, 'a']);
                $dsc->setDescriptor($dsc::STDERR, [$dsc::TYPE_FILE, $errorLog, 'a']);
                $cmdObj->setDescritorSpecifications($dsc->getDescriptorMapping());
                $cmdObj->setOtherOptions(['bypass_shell' => true]);
                $cmdObj->start();

                $this->log(' pid = ' . $cmdObj->getStatusPid(), false, true);

                $this->running[$processKey . '_' . $i] = $cmdObj;
                $this->errorCounts[$processKey . '_' . $i] = 0;
            }
        }

        $this->log('ALL RUNNING!');

        $isRunning = true;

        while ($isRunning) {
            $isRunning = false;

            foreach ($this->running as $key => $cmd) {
                if ($cmd->getStatusIsRunning()) {
                    $isRunning = true;
                }
                else {
                    $oldPid = $cmd->getStatusPid();
                    $cmd->start();
                    $this->log('Restarted: ' . $key . ' (' . $oldPid . ' -> ' . $cmd->getStatusPid() . ')');
                }
            }

            sleep(5);
        }

        foreach ($this->running as $cmd) {
            $cmd->end();
        }

        $this->log('ALL STOPPED!');
    }

    protected function getProcessField ($process, $field, $default = null) {
        return array_key_exists($field, $process) ? $process[$field] : $default;
    }

    protected function getProcessCount ($process) {
        $cnt = $this->getProcessField($process, 'instance_count');

        if ($cnt && is_numeric($cnt) && $cnt > 0) {
            return round($cnt);
        }

        return 1;
    }

    protected function log ($message, $prefix = true, $suffix = true) {
        if ($prefix) {
            echo date('Y-m-d H:i:s') . ' ';
        }

        echo $message;

        if ($suffix) {
            echo PHP_EOL;
        }
    }

    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array
     */
    public function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }

        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }
}
